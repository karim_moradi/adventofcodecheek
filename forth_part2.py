#!/usr/local/bin/python

import sys

class board():
    mat = None
    mark = None
    win = False
    def __init__(self, nums0, nums1, nums2, nums3, nums4):
        #print(type(nums0), nums0)
        #print(nums0.split())
        #sys.stdin.read(1)
        self.mat = [nums0.split(), nums1.split(), nums2.split(), nums3.split(), nums4.split()]
        self.mark = [[1,1,1,1,1], [1,1,1,1,1], [1,1,1,1,1], [1,1,1,1,1], [1,1,1,1,1]]

    def won(self):
        self.win = True

    def marker(self, mark):
        for i in range(5):
            for j in range(5):
                #print("\ti*j: '%s'" % type(self.mark[i][j]))
                #print("\tmark: '%s'" % type(mark))
                #sys.stdin.read(1)
                if self.mat[i][j] == mark:
                    self.mark[i][j] = "0"
                    #print(self.mark[i][j])

    def checker(self):
        for i in range(5):
            marksum = 0
            for j in range(5):
                marksum += int(self.mark[i][j])
            if marksum == 0:
                numsum = 0
                for i in range(5):
                    for j in range(5):
                        numsum += int(self.mark[i][j]) * int(self.mat[i][j])
                return numsum
        for i in range(5):
            marksum = 0
            for j in range(5):
                marksum += int(self.mark[j][i])
            if marksum == 0:
                numsum = 0
                for i in range(5):
                    for j in range(5):
                        numsum += int(self.mark[i][j]) * int(self.mat[i][j])
                return numsum
        return 0

boards = {}
numbers = []
def load():
    #datafd = open("forth_data_test.txt")
    datafd = open("forth_data.txt")
    data = datafd.read().splitlines()
    global numbers
    numbers = list(data[0].split(","))
    i = 2
    ith_board = 0
    while True:
        #print(data[i])
        #sys.stdin.read(1)
        if data[i] == '':
            i += 1
            continue
        boards[ith_board] = board(data[i], data[i+1], data[i+2], data[i+3], data[i+4])
        i += 5
        if i >= len(data):
            break
        ith_board += 1

load()
#for i in range(len(boards)):
#    print(boards[i].mat)
#    print()
#print(numbers)
number_of_boards = len(boards)
number_of_winners = 0
for mark in numbers:
    print(mark)
    for j in range(len(boards)):
        boards[j].marker(mark)
        checker_res = boards[j].checker()
        if checker_res != 0:
            print(boards[j].mat)
            print(boards[j].mark)
            print(checker_res * int(mark))
            if not boards[j].win:
                boards[j].won()
                number_of_winners += 1
                if number_of_boards == number_of_winners:
                    print("this is the last winner!")
                    sys.stdin.read(1)

#print(boards[0].mat)
