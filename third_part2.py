#!/usr/local/bin/python

import sys
gamma = ''
epsilon = ''
datafd = open("third_data.txt")
data = datafd.read().splitlines()
oxygen = []
co2 = []
for i in range(0, 12):
    zero = one = 0
    if len(data) == 1:
        break
    for item in data:
        if (item[i]) == '1':
            one += 1
        else:
            zero += 1
    if one >= zero:
        for item in data:
            if item[i] == '1':
                oxygen.append(item)
    else:
        for item in data:
            if item[i] == '0':
                oxygen.append(item)
    #print(oxygen)
    data = oxygen
    oxygen = []
ox = int(data[0], 2)
datafd.seek(0, 0)
data = datafd.read().splitlines()
for i in range(0, 12):
    zero = one = 0
    if len(data) == 1:
        break
    for item in data:
        if (item[i]) == '1':
            one += 1
        else:
            zero += 1
    if one < zero:
        for item in data:
            if item[i] == '1':
                co2.append(item)
    else:
        for item in data:
            if item[i] == '0':
                co2.append(item)
    #print(co2)
    #sys.stdin.read(1)
    data = co2
    co2 = []
co = int(data[0], 2)
print(ox * co)
