#!/usr/local/bin/python

import sys

forward = 0
depth = 0
with open("secondday_data.txt") as datafd:
    for inp in datafd:
        if inp.startswith("forward"):
            forward += int(inp.split(" ", 1)[1])
        elif inp.startswith("down"):
            depth += int(inp.split(" ", 1)[1])
        elif inp.startswith("up"):
            depth -= int(inp.split(" ", 1)[1])

print(depth * forward)
