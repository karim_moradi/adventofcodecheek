#!/usr/local/bin/python

import sys

hor_pos = 0
depth = 0
aim = 0
with open("secondday_data.txt") as datafd:
    for inp in datafd:
        if inp.startswith("forward"):
            hor_pos += int(inp.split(" ", 1)[1])
            depth += aim * int(inp.split(" ", 1)[1])
        elif inp.startswith("down"):
            aim += int(inp.split(" ", 1)[1])
        elif inp.startswith("up"):
            aim -= int(inp.split(" ", 1)[1])

print(depth * hor_pos)
