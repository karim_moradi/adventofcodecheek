#!/usr/local/bin/python

import sys
gamma = ''
epsilon = ''
with open("third_data.txt") as datafd:
    idx = 0
    for i in range(0, 12):
        zero = one = 0
        datafd.seek(0, 0)
        for inp in datafd:
            if (inp[i]) == '1':
                one += 1
            else:
                zero += 1
        if one >= zero:
            gamma += '1'
            epsilon += '0'
        else:
            gamma += '0'
            epsilon += '1'
        print(i)
print(int(gamma, 2) * int(epsilon, 2))
